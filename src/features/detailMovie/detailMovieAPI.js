import axios from "axios";
const API_KEY = process.env.REACT_APP_API_KEY; // api key ada di file .env yang termasuk gitignore


export const getDetailMovie = (movieId) => {
    return axios.get(`https://api.themoviedb.org/3/movie/${movieId}?api_key=${API_KEY}&language=en-US`);
}
export const getCreditDetailMovie = (movieId) => {
    return axios.get(`https://api.themoviedb.org/3/movie/${movieId}/credits?api_key=${API_KEY}&language=en-US`);
}
export const getVideoDetailMovie = (movieId) => {
    return axios.get(`https://api.themoviedb.org/3/movie/${movieId}/videos?api_key=${API_KEY}&language=en-US`);
}