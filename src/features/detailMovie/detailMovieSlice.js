import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isLoading: false,
    isError: false,
    data: {},
    credit: {},
    videos: []
}

const detailMovieFetchInitialization = (state) => {
    state.isLoading = true;
    state.isError = false;
}

const detailMovieFetchSuccess = (state, { payload }) => {
    state.isLoading = false;
    state.data = payload;
    // Tambah data payload ke state
}

const detailMovieFetchFailed = (state) => {
    state.isLoading = false;
    state.isError = true;
}

const creditMovieFetchSuccess = (state, { payload }) => {
    state.isLoading = false;
    state.credit = payload;
}

const videoMovieFetchSuccess = (state, { payload }) => {
    state.isLoading = false;
    state.videos = payload.results
}

const detailMovieSlice = createSlice({
    name: "detail/movie",
    initialState,
    reducers: {
        detailMovieFetchInitialization,
        detailMovieFetchSuccess,
        detailMovieFetchFailed,
        creditMovieFetchSuccess,
        videoMovieFetchSuccess
    }
})

export const detailMovieActions = detailMovieSlice.actions;

export default detailMovieSlice.reducer;