import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../features/login/authSlice';
import movieReducer from '../features/movie/movieSlice';
import detailMovieReducer from '../features/detailMovie/detailMovieSlice'

export const store = configureStore({
  reducer: {
    auth: authReducer,
    movie: movieReducer,
    detailMovie: detailMovieReducer
  },
});
