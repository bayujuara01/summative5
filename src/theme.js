import { createTheme } from "@mui/material";
import { blue } from "@mui/material/colors";

const theme = createTheme({
    palette: {
        primary: {
            main: blue[700]
        }
    },
    myButton: {
        backgroundColor: "red",
        color: "white",
        border: "1px solid black",
    },
});

export default theme;