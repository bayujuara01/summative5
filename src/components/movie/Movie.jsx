
import React from 'react';
import { Rating, Grid, Card, CardMedia, CardContent, Typography, CardActions, Button } from '@mui/material';
import { Link } from 'react-router-dom';

const Item = ({ movie }) => {

    return <>
        <Grid item xl={2} md={3} sm={6} xs={12} >
            <Card sx={{ maxWidth: 240, height: 450 }}>
                {movie.poster_path ? <>
                    <CardMedia
                        component="img"
                        alt={`poster of ${movie.title}`}
                        style={{ height: '16rem', objectFit: 'cover' }}
                        image={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
                    />
                </> : <>
                    <CardMedia

                        component="img"
                        style={{ height: '21rem', maxWidth: "90%", display: "inline-block" }}
                        alt="no poster"
                        src="https://748073e22e8db794416a-cc51ef6b37841580002827d4d94d19b6.ssl.cf3.rackcdn.com/not-found.png"
                    />
                </>}

                <CardContent>
                    <Typography variant="h6" >
                        {movie.title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Subtitle
                    </Typography>
                    <Rating name="read-only" value={Math.round(movie.vote_average / 2.0)} readOnly />
                </CardContent>
                <CardActions>
                    <Link to={`/movie/${movie.id}`} style={{ textDecoration: 'none' }}>
                        <Button variant="contained" size="small">Detail</Button>
                    </Link>
                </CardActions>
            </Card>
            {/* <Zoom in={true} timeout={5000}> */}

            {/* </Zoom> */}
        </Grid>

    </>;
}

export default Item;