import { useEffect, useState } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { FETCH_DISCOVER_URL, FETCH_TRENDING_URL } from "../features/movie/movieAPI";
import { movieActions } from "../features/movie/movieSlice";
import List from '../components/movie/List';
import ListHorizontal from "../components/movie/ListHorizontal";
import { Typography, Box } from "@mui/material";

const Home = () => {
    const POPULAR_DESC = `&page=1&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false`;
    const [url, setUrl] = useState(`${FETCH_DISCOVER_URL}${POPULAR_DESC}`);

    const dispatch = useDispatch();
    const { movie } = useSelector(state => state);

    useEffect(() => {
        async function fetchMovie() {
            dispatch(movieActions.movieFetchInitialization());

            try {
                const result = await axios.get(url);
                const trendResult = await axios.get(FETCH_TRENDING_URL);
                // console.log(trendResult);
                dispatch(movieActions.movieFetchSuccess(result.data));
                dispatch(movieActions.movieTrendFetchSuccess(trendResult.data));

            } catch (err) {
                dispatch(movieActions.movieFetchFailed());
            }
        }

        fetchMovie();
    }, [dispatch, url]);

    return <>
        <Box>
            <Typography variant="h5" fontWeight="bold">
                Trending Now 🔥
            </Typography>
            <ListHorizontal movies={movie.trends} />
        </Box>
        <Typography variant="h5" gutterBottom fontWeight="bold">
            Popular Now 🔥
        </Typography>
        <List movies={movie.data} />
    </>;
}

export default Home;