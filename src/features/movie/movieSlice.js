import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isLoading: false,
    isError: false,
    page: 1,
    resultsCount: 0,
    data: [],
    searchTerm: '',
    trends: []
}

const movieFetchInitialization = (state) => {
    state.isLoading = true;
    state.isError = false;
}

const movieFetchSuccess = (state, { payload }) => {
    state.isLoading = false;
    state.page = 1;
    state.resultsCount = payload.total_results;
    state.data = payload.results;
    // console.log(payload);
    // Tambah data payload ke state
}

const movieFetchFailed = (state) => {
    state.isLoading = false;
    state.isError = true;
}

const movieTrendFetchSuccess = (state, { payload }) => {
    state.isLoading = false;
    state.trends = payload.results;
}

const movieSlice = createSlice({
    name: "movie",
    initialState,
    reducers: {
        movieFetchInitialization,
        movieFetchSuccess,
        movieFetchFailed,
        movieTrendFetchSuccess
    }
})

export const movieActions = movieSlice.actions;

export default movieSlice.reducer;