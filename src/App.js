import { Grid, ThemeProvider } from '@mui/material';
import React from 'react';
import theme from './theme'
import './App.css';
import Navbar from './components/Navbar';
import Drawer from './components/Drawer';
import Auth from './features/login/Auth';
import { useSelector } from 'react-redux';
import Home from './screens/Home';
import DetailMovie from './screens/DetailMovie';
import { Routes, Route } from 'react-router';

function App() {

  const { auth } = useSelector(state => state);

  return <>
    <ThemeProvider theme={theme} >
      {auth.isAuth ? <>
        <Navbar />
        <Grid container>
          <Grid item sm={2} xs={2}>
            <Drawer />
          </Grid>
          <Grid item sm={10} xs={10} marginTop="80px" paddingLeft="15px" paddingRight="15px">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/movie/:id" element={<DetailMovie />} />
            </Routes>
          </Grid>
        </Grid>
      </> : <>
        <Auth />
      </>}

    </ThemeProvider>
  </>;
}

export default App;
