import { useLocation } from 'react-router';
import {
    Grid,
    Box,
    useTheme,
    Typography,
    ImageList,
    ImageListItem,
    CardMedia,
    Card,
    Chip,
    Stack
} from '@mui/material';
import StarsRoundedIcon from '@mui/icons-material/StarsRounded';
import { useDispatch, useSelector } from 'react-redux';
import { detailMovieActions } from '../features/detailMovie/detailMovieSlice';
import { getDetailMovie, getCreditDetailMovie, getVideoDetailMovie } from '../features/detailMovie/detailMovieAPI'
import { useEffect } from 'react';
import { grey, yellow } from '@mui/material/colors';
import YouTube from 'react-youtube';
import Casts from '../components/movie/Casts';

const useStyle = (theme) => ({
    hero: {
        width: '100%',
    },
    title: {
        marginBottom: '10px',
        paddingLeft: '0px'
    },
    pargraph: {
        paddingLeft: '0px'
    },
    genreList: {
        marginTop: '14px'
    },
    vote: {
        marginLeft: '8px'
    },
    reviewed: {
        marginLeft: '10px'
    }
});

const DetailMovie = () => {
    const [, , movieId] = useLocation().pathname.split('/');
    const theme = useTheme;
    const classes = useStyle(theme);

    const { detailMovie } = useSelector(state => state);
    const dispatch = useDispatch();

    useEffect(() => {
        async function fetchDetailMovie() {
            dispatch(detailMovieActions.detailMovieFetchInitialization());
            try {
                const result = await getDetailMovie(movieId);
                // console.log(result);
                dispatch(detailMovieActions.detailMovieFetchSuccess(result.data));
            } catch (err) {
                dispatch(detailMovieActions.detailMovieFetchFailed());
            }
        }

        async function fetchCreditMovie() {
            const result = await getCreditDetailMovie(movieId);
            dispatch(detailMovieActions.creditMovieFetchSuccess(result.data))
        }

        async function fetchVideoMovie() {
            const result = await getVideoDetailMovie(movieId);
            dispatch(detailMovieActions.videoMovieFetchSuccess(result.data));
        }

        fetchDetailMovie();
        fetchCreditMovie();
        fetchVideoMovie();
    }, [dispatch, movieId]);

    return <>
        {!detailMovie.isLoading && <>
            <Grid container spacing={2}>
                <Grid item xs={6} md={12}>
                    <Box sx={classes.hero}>
                        <ImageList cols={1}>
                            <ImageListItem>
                                <img
                                    style={{ borderRadius: '8px' }}
                                    src={`https://image.tmdb.org/t/p/w780${detailMovie.data.backdrop_path}`}
                                    alt="t"
                                    loading="lazy" />
                                {/* <ImageListItemBar
                            title={tile.title}
                        /> */}
                            </ImageListItem>
                        </ImageList>
                    </Box>

                </Grid>
                <Grid item xs={6} md={4} sx={{
                    paddingLeft: '20px'
                }}>
                    <Card >
                        {detailMovie.data.poster_path ? <>
                            <CardMedia
                                component="img"
                                alt={`poster of `}
                                style={{ height: "480px", objectFit: 'cover', maxWidth: "100%" }}
                                src={`https://image.tmdb.org/t/p/w500${detailMovie.data.poster_path}`}
                            />
                        </> : <>
                            <CardMedia

                                component="img"
                                style={{ height: "480px", maxWidth: "100%", display: "inline-block" }}
                                alt="no poster"
                                src="https://image.tmdb.org/t/p/w500/d5NXSklXo0qyIYkgV94XAgMIckC.jpg"
                            />
                        </>}
                    </Card>
                </Grid>
                <Grid item md={8} sx={{
                    paddingRight: '20px'
                }}>
                    <Typography variant="h3" sx={classes.title}>{detailMovie.data.original_title}</Typography>
                    <Typography variant="p" sx={classes.pargraph}>{detailMovie.data.overview}</Typography>
                    <Stack direction="row" spacing={1} sx={classes.genreList}>
                        {detailMovie.data.genres && detailMovie.data.genres.map(genre => <Chip key={genre.id} label={genre.name} color="primary" />)}
                    </Stack>
                    <Grid container>
                        <Grid item md={6}>
                            <Stack direction="row" alignItems="center" sx={{
                                marginTop: '50px'
                            }}>
                                <StarsRoundedIcon sx={{
                                    color: yellow[700],
                                    fontSize: 80
                                }} />
                                <Typography variant="h3" sx={classes.vote}>{detailMovie.data.vote_average}</Typography>
                                <Typography variant="p" sx={classes.reviewed} color={grey[800]} >({detailMovie.data.vote_count} reviewed)</Typography>
                            </Stack>
                        </Grid>
                        <Grid item md={6}>T</Grid>
                    </Grid>
                </Grid>
                {detailMovie.videos[0] && <Grid item md={12}>
                    <Typography variant="h5">Trailer 🎥</Typography>
                    <Stack direction="row" justifyContent="center">
                        <YouTube videoId={detailMovie.videos[0].key} />
                    </Stack>
                </Grid>}
                {detailMovie.credit.cast && <>
                    <Typography variant="h5">Actor</Typography>
                    <Grid item md={12}>
                        <Casts casts={detailMovie.credit.cast} />
                    </Grid>
                </>}

            </Grid>
        </>}
    </>;
}

export default DetailMovie;