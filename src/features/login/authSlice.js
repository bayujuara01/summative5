import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    isAuth: false,
    username: '',
    password: ''
}

const login = (state, { payload }) => {
    if (payload.username === 'bayujuara01' && payload.password === 'bayu1234') {
        state.isAuth = true;
    } else {
        alert("Login Salah");
    }
}

const logout = (state) => {
    state.isAuth = false;
}

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        login,
        logout
    }
});

export const authActions = authSlice.actions;

export default authSlice.reducer;
