import { Grid, Fade } from '@mui/material';
import Movie from './Movie';


const List = ({ movies }) => {
    return <>
        <Fade in={true} timeout={1000}>
            <Grid container spacing={2}>
                {movies &&
                    movies
                        .map((movie, index) => (
                            <Movie key={index} movie={movie} />
                        ))}
            </Grid>
        </Fade>
    </>;
};

export default List;