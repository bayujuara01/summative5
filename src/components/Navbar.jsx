import { AppBar, Avatar, Badge, Box, InputBase, Toolbar, Typography, LinearProgress } from "@mui/material";
import { useTheme } from '@mui/material/styles';
import { Cancel, Favorite, Notifications, Search } from '@mui/icons-material';
import { alpha } from "@mui/system";
import { useState } from "react";
import { useSelector } from "react-redux";

const useStyle = (theme, props) => ({
    toolbar: {
        display: "flex",
        justifyContent: "space-between",
    },
    logoLarge: {
        display: "none",
        [theme.breakpoints.up("sm")]: {
            display: "block"
        }
    },
    logoSmall: {
        display: "block",
        [theme.breakpoints.up("sm")]: {
            display: "none"
        }
    },
    search: {
        display: "flex",
        paddingLeft: '10px',
        alignItems: "center",
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        "&:hover": {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        borderRadius: theme.shape.borderRadius,
        width: "50%",
        [theme.breakpoints.down("sm")]: {
            display: props.open ? "flex" : "none",
            width: "70%",
        },
    },
    input: {
        width: "100%",
        color: "white",
        marginLeft: theme.spacing(1),
    },
    cancel: {
        [theme.breakpoints.up("sm")]: {
            display: "none",
        },
    },
    searchButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up("sm")]: {
            display: "none",
        },
    },
    icons: {
        alignItems: "center",
        display: props.open ? "none" : "flex",
    },
    badge: {
        marginRight: theme.spacing(2),
    },
});

const Navbar = () => {

    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const classes = useStyle(theme, { open });

    const { movie, detailMovie } = useSelector(state => state);

    return <>
        <AppBar>
            <Toolbar sx={classes.toolbar}>
                <Typography variant="h6" sx={classes.logoLarge} >Bubui Movie</Typography>
                <Typography variant="h6" sx={classes.logoSmall}>Bubui Movie 2</Typography>
                <Box sx={classes.search}>
                    <Search />
                    <InputBase sx={classes.input} placeholder="Search..." />
                    <Cancel
                        sx={classes.cancel}
                        onClick={() => setOpen(false)} />
                </Box>
                <Box sx={classes.icons}>
                    <Search
                        sx={classes.searchButton}
                        onClick={() => setOpen(true)}
                    />
                    <Badge badgeContent={0} color="secondary" sx={classes.badge}>
                        <Favorite />
                    </Badge>
                    <Badge badgeContent={2} color="secondary" sx={classes.badge}>
                        <Notifications />
                    </Badge>
                    <Avatar
                        alt="Bayu SenoAriefyanto"
                        src="https://avatars1.githubusercontent.com/u/18959203?v=4"
                    />
                </Box>
            </Toolbar>
            {(movie.isLoading || detailMovie.isLoading) && <LinearProgress color="inherit" />}
        </AppBar>
    </>
}

export default Navbar;