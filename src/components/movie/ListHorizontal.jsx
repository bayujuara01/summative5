import { Box, ImageList, ImageListItem, ImageListItemBar, useTheme } from '@mui/material';


const useStyles = theme => ({
    imageList: {
        width: '100%',

    }
});

export default function ListHorizontal({ movies }) {
    const theme = useTheme()
    const classes = useStyles(theme);
    return (
        <Box>
            <ImageList sx={classes.imageList} cols={7}>
                {movies.slice(0, 7).map((movie, index) => (
                    <ImageListItem key={index} sx={{ flexWrap: 'wrap' }}>
                        <img src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`} alt={movie.title} />
                        <ImageListItemBar
                            title={movie.title}
                        />
                    </ImageListItem>
                ))}
            </ImageList>
        </Box>
    );
}
