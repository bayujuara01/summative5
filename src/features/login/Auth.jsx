import { useState } from 'react'
import { Grid, Paper, Avatar, TextField, Button, Checkbox, FormControlLabel } from '@mui/material';
import { LockOutlined } from '@mui/icons-material';
import { authActions } from './authSlice';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';

const Auth = () => {
    const paperStyle = { padding: 20, height: '65vh', width: 300, margin: "50px auto" };
    const avatarStyle = { backgroundColor: '#1bbd7e', marginTop: '14px' };
    const btnstyle = { margin: '10px 0' };
    const fieldStyle = { margin: '10px 0' };

    const navigate = useNavigate()
    const dispatch = useDispatch();

    const [user, setUser] = useState({
        username: '',
        password: ''
    });

    const handleFormChange = (events) => {
        setUser({
            ...user,
            [events.target.name]: events.target.value
        })
    }

    const handleSubmit = (events) => {
        events.preventDefault();
        dispatch(authActions.login(user));
        navigate("/");
    }

    return <>
        <Grid>
            <Paper style={paperStyle}>
                <Grid align='center'>
                    <Avatar style={avatarStyle}><LockOutlined /></Avatar>
                    <h2>Sign In</h2>
                </Grid>
                <TextField
                    name="username"
                    onChange={handleFormChange}
                    label='Username'
                    style={fieldStyle}
                    placeholder='Enter username' fullWidth required />
                <TextField
                    name="password"
                    onChange={handleFormChange}
                    label='Password'
                    placeholder='Enter password'
                    style={fieldStyle}
                    type='password' fullWidth required />
                <FormControlLabel
                    control={
                        <Checkbox
                            name="checkedB"
                            color="primary"
                        />
                    }
                    label="Remember me"
                />
                <Button type='submit' onClick={handleSubmit} color='primary' variant="contained" style={btnstyle} fullWidth>Sign in</Button>
            </Paper>
        </Grid>
    </>;
}

export default Auth;