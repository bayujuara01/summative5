import { Box, ImageList, ImageListItem, ImageListItemBar, useTheme } from '@mui/material';


const useStyles = theme => ({
    imageList: {
        width: '100%',

    }
});

export default function Casts({ casts }) {
    const theme = useTheme()
    const classes = useStyles(theme);
    return (
        <Box>
            <ImageList sx={classes.imageList} cols={7}>
                {casts.slice(0, 7).map((cast, index) => (
                    <ImageListItem key={index} sx={{ flexWrap: 'wrap' }}>
                        <img src={`https://image.tmdb.org/t/p/w500${cast.profile_path}`} alt={cast.name} />
                        <ImageListItemBar
                            title={cast.name}
                        />
                    </ImageListItem>
                ))}
            </ImageList>
        </Box>
    );
}
