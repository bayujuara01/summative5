const API_KEY = process.env.REACT_APP_API_KEY; // api key ada di file .env yang termasuk gitignore
const FETCH_DISCOVER_URL = `https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}`;
const FETCH_POPULAR_URL = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}`;
const FETCH_TRENDING_URL = `https://api.themoviedb.org/3/trending/movie/week?api_key=${API_KEY}`;
const FETCH_SEARCH_URL = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&page=1&include_adult=false&query=`;

module.exports = {
    API_KEY,
    FETCH_DISCOVER_URL,
    FETCH_POPULAR_URL,
    FETCH_TRENDING_URL,
    FETCH_SEARCH_URL
}

