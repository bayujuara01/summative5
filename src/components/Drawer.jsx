import { Box, Container, Typography, useTheme } from "@mui/material";
import {
    List,
    ExitToApp,
    Home
} from "@mui/icons-material";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { authActions } from '../features/login/authSlice';

const useStyles = (theme) => ({
    container: {
        height: "100vh",
        color: "white",
        paddingTop: theme.spacing(10),
        backgroundColor: theme.palette.primary.main,
        position: "sticky",
        top: 0,
        [theme.breakpoints.up("sm")]: {
            backgroundColor: "white",
            color: "#555",
            border: "1px solid #ece7e7",
        },
    },
    item: {
        display: "flex",
        alignItems: "center",
        marginBottom: theme.spacing(4),
        [theme.breakpoints.up("sm")]: {
            marginBottom: theme.spacing(3),
            cursor: "pointer",
        },
    },
    icon: {
        marginRight: theme.spacing(1),
        [theme.breakpoints.up("sm")]: {
            fontSize: "18px",
        },
    },
    text: {
        fontWeight: 500,
        [theme.breakpoints.down("sm")]: {
            display: "none",
        },
    },
});

const Drawer = () => {
    const theme = useTheme();
    const classes = useStyles(theme);

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const onLogoutClick = () => {
        dispatch(authActions.logout());
        navigate("/");
    }

    return (
        <Container sx={classes.container}>
            <Link to="/">
                <Box sx={classes.item}>
                    <Home sx={classes.icon} />
                    <Typography sx={classes.text}>Home</Typography>
                </Box>
            </Link>
            <Box sx={classes.item}>
                <List sx={classes.icon} />
                <Typography sx={classes.text}>Lists</Typography>
            </Box>
            <Box sx={classes.item} onClick={onLogoutClick} >
                <ExitToApp sx={classes.icon} />
                <Typography sx={classes.text}>Logout</Typography>
            </Box>
        </Container>
    );
};

export default Drawer;